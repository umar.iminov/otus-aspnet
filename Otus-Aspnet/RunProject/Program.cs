﻿using (var context = new DataContext())
{
    var dbInitializer = new EfDbInitializer(context);
    dbInitializer.InitializeDb();
    Console.WriteLine("Done");
}