﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Aspnet.Core.Domain.Administration;
using Otus.Aspnet.Core.Domain.InventoryManagement;

namespace Otus.Aspnet.DataAccess.Data
{
    internal class DefaultDataFactory
    {

        public static IEnumerable<User> Users => new List<User>()
        {
            new User()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "umar.iminov@gmail.com",
                FirstName = "Умар",
                LastName = "Иминов",
                Role = Roles.FirstOrDefault(x => x.Name == "Admin"),

            },
            new User()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                Role = Roles.FirstOrDefault(x => x.Name == "User"),

            },
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "User",
                Description = "Пользователь"
            }
        };

        public static IEnumerable<InventoryItem> InventoryItems => new List<InventoryItem>()
        {
            new InventoryItem()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                InventoryCode = "0000001",
                InventoryName = "ПК HP",
                Description = "Персональный компьютер",
                CreatedById = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f")

            },
             new InventoryItem()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                InventoryCode = "0000002",
                InventoryName = "Кресло офисное",
                Description = "Кресло офисное",
                CreatedById = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f")

            },
              new InventoryItem()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                InventoryCode = "000003",
                InventoryName = "Монитор Dell",
                Description = "Монитор Dell 24",
                CreatedById = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f")

            }
          };


        public static IEnumerable<InventoryHistory> InventoryHistories => new List<InventoryHistory>()
            {

                    new InventoryHistory()
                    {
                        InventoryItemId =    Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                        InventoryUserId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895")

                    },
                    new InventoryHistory()
                    {
                        InventoryItemId =    Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                        InventoryUserId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895")
                    }



            };



}
        
    }