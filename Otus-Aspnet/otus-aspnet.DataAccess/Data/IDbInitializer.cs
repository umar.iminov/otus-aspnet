﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Aspnet.DataAccess.Data
{
    internal interface IDbInitializer
    {
        public void InitializeDb();
    }
}
