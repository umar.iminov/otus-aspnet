﻿using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Otus.Aspnet.DataAccess.Data
{

            public class EfDbInitializer
            : IDbInitializer
        {
            private readonly DataContext _dataContext;

            public EfDbInitializer(DataContext dataContext)
            {
                _dataContext = dataContext;
            }

            public void InitializeDb()
            {
                //_dataContext.Database.EnsureDeleted();
                //_dataContext.Database.EnsureCreated();
                _dataContext.Database.Migrate();


                if (!_dataContext.Users.Any())
                {
                    _dataContext.AddRange(DefaultDataFactory.Users);
                    _dataContext.SaveChanges();
                }
                if (!_dataContext.Roles.Any())
                {
                    _dataContext.AddRange(DefaultDataFactory.Roles);
                    _dataContext.SaveChanges();
                }
                if (!_dataContext.InventoryItems.Any())
                {
                    _dataContext.AddRange(DefaultDataFactory.InventoryItems);
                    _dataContext.SaveChanges();
                }
                if (!_dataContext.InventoryHistories.Any())
                {
                    _dataContext.AddRange(DefaultDataFactory.InventoryHistories);
                    _dataContext.SaveChanges();
                }


            }
        }
    }