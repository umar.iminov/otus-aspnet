﻿using Microsoft.EntityFrameworkCore;
using Otus.Aspnet.Core.Domain.Administration;
using Otus.Aspnet.Core.Domain.InventoryManagement;
using Otus.Aspnet.DataAccess.Data;
using System.Collections.Generic;
using System.Configuration;

namespace Otus.Aspnet.DataAccess
{
            public class DataContext : DbContext
        {
            public DbSet<InventoryItem> InventoryItems { get; set; }

            public DbSet<Role> Roles { get; set; }

            public DbSet<User> Users { get; set; }

            public DbSet<InventoryHistory> InventoryHistories { get; set;}

        public DataContext()
        { 
        
        }
    
            public DataContext(DbContextOptions<DataContext> options)
               : base(options)
            {


            }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            //string connectionString = ConfigurationManager.AppSettings.Get("ConnectionString");

            string connectionString = "Host = localhost; Port = 5432; Database = InventoryDB; Username = postgres; Password = Start123";

            optionsBuilder.UseNpgsql(connectionString);
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
            {

              modelBuilder.Entity<InventoryItem>()
                .HasKey(a => a.Id);
                            
            modelBuilder.Entity<InventoryItem>()
                .HasMany(b => b.Users)
                .WithMany(a => a.Items)
                .UsingEntity <InventoryHistory> 
                (
                 j => j.HasOne(u => u.InventoryUser)
                 .WithMany(i => i.InventoryHistories)
                 .HasForeignKey(u => u.InventoryUserId), 

                j => j.HasOne(i => i.InventoryItem)
                .WithMany(u => u.InventoryHistories)
                .HasForeignKey(a => a.InventoryItemId),
                j =>
                {
                    j.Property(pt => pt.DateStart).HasDefaultValueSql("CURRENT_TIMESTAMP");
                    j.ToTable("InventoryHistory");
                }

                );
             
            }

        }
    }
