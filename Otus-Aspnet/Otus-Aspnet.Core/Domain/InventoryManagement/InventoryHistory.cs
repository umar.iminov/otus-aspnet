﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Aspnet.Core.Domain.Administration;

namespace Otus.Aspnet.Core.Domain.InventoryManagement
{
    public class InventoryHistory: BaseEntity
    {
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public User InventoryUser  { get; set; }
        public Guid InventoryUserId { get; set; }

        public InventoryItem InventoryItem { get; set; }
        public Guid InventoryItemId { get; set; }
        
        public bool IsConfirmed { get; set; }
        public string Remark { get; set; }

    }
}
