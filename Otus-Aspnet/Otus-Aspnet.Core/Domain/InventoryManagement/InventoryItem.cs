﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Aspnet.Core.Domain;
using Otus.Aspnet.Core.Domain.Administration;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Aspnet.Core.Domain.InventoryManagement
{
    public class InventoryItem : BaseEntity
    {
        public string InventoryCode { get; set; }
        public string InventoryName { get; set; }
        public string Description { get; set; }
        [NotMapped]
        public User CreatedBy { get; set; }
        public Guid CreatedById { get; set; }

        public ICollection<User> Users { get; set; }
        public ICollection<InventoryHistory> InventoryHistories { get; set; }

    }
}
