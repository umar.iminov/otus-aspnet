﻿using System;

namespace Otus.Aspnet.Core.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}