﻿using System;
using System.Collections.Generic;
using Otus.Aspnet.Core.Domain.InventoryManagement;

namespace Otus.Aspnet.Core.Domain.Administration
{
    public class User
        : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Email { get; set; }

        public Role Role { get; set; }

        public Guid RoleId { get; set; }

        public ICollection<InventoryItem> Items { get; set; }
        public ICollection<InventoryHistory> InventoryHistories { get; set; }
                
    }
}